package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileProcessor {

    public static void printContents(String fileName){
        ConfigurationSettingsSingleton fileNameEnum = ConfigurationSettingsSingleton.INSTANCE;
        String url = fileNameEnum.getUrl() + fileName;
        File file = new File(url);

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr);) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
