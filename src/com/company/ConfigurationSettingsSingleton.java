package com.company;

public enum ConfigurationSettingsSingleton {
    INSTANCE;

    String url;

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }



}
