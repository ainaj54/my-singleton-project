package com.company;

import static com.company.FileProcessor.printContents;

public class Main {

    public static void  main(String[] args) {
        ConfigurationSettingsSingleton mySingleton = ConfigurationSettingsSingleton.INSTANCE;
        System.out.println(mySingleton.getUrl());
        mySingleton.setUrl("C:\\Users\\ajop87\\Idea Projects\\Assignments\\Singleton-patterns\\src\\com\\company\\");
        System.out.println(mySingleton.getUrl());

        printContents("file.txt");
        printContents("file2.txt");

    }
};

